(function( $ ){
    jQuery.validator.addMethod("allPhoneFormat", function (value, element) {
		return this.optional(element) || /^[0-9()\-+ ]{7,}$/.test(value);
	}, "Введите корректно.");
    $.fn.staFeedback = function(options) {
        
        var settings = $.extend({}, options);

        var thisForm = this;

        this.validate(
            {
                rules: {
                    name: {
                        required:true
                    },
                    phone:{
                        required: true,
                        allPhoneFormat: true
                    },
                    email:{
                        required: true,
                        email:true
                    }
                },

                messages:{
                    email:{
                        required: "Введите E-mail",
                        email: "Неверный формат"
                    },
                    phone:{
                        required: "Введите телефон",
                        allPhoneFormat: "Введите корректно"
                    },
                    name: {
                        required:"Введите имя"
                    }
                },

                ignore: ".ignore",

                submitHandler: submit
            }
        );
        //this.find("input[name='phone']").mask("?(999) 999-99-99");

        function submit(form){
            console.log($(form));
            $.post(
                'http://emailsend.u0028271.plsk.regruhosting.ru/index.php?from=dgu',
                $(form).serialize(),
                parseResponce);
        }

        function parseResponce(response) {
            thisForm.trigger('reset');
            $(".top-form-send-status").css({display:"block"});
            //thisForm.replaceWith('<p>Ваша заявка<br>успешно отправлена</p><p>наш менеджер свяжется с вами<br>в ближайшее время</p><img src="/images/thank_icon.png" alt=""><p class="big-p">Спасибо, что выбрали нас!</p>')
            //$(".modal-order").css("opacity", 0).hide();
            //$(".modal-thx").css({opacity:1, top:"50%"}).show();
        }
    };
})(jQuery);